package at.haem.car;

public class Motor {
	
	private String identification;
	private String operatingLiquid;
	private int performance;
	
	
	public Motor(String identification, String operatingLiquid, int performance) {
		this.identification = identification;
		this.operatingLiquid = operatingLiquid;
		this.performance = performance;
		
	}
	
	public String getIdentification(){
		return this.identification;
	}

	public String getOperatingLiquid() {
		return operatingLiquid;
	}

	public void setOperatingLiquid(String operatingLiquid) {
		this.operatingLiquid = operatingLiquid;
	}
	
	
	
}
