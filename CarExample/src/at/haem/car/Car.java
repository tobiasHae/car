package at.haem.car;

public class Car {
	
	private String colour;
	private int maxV;
	private int Price;
	private double usage;
	private String producername;
	private String motor;
	private int mileage;
	private Motor engine;
	private Producer produc;
	
	
	public Car(String colour, int maxV, int Price, double usage, String producername, String motor, int mileage, Motor engine, Producer produc) {
		this.colour = colour;
		this.maxV = maxV;
		this.Price = Price;
		this.usage = usage;
		this.producername = producername;
		this.motor = motor;
		this.mileage = mileage;
		this.engine = engine;
		this.produc = produc;
	}
	
	
	public void showProducer() {
		System.out.println("My producer is:" + this.producername);
	}
	
	public void showMotor() {
		System.out.println("My motor is:" + this.motor);
	}
	
	
	public int getPrice() {
		int temp = this.Price-(this.Price /100 * this.produc.getDiscount());
		return temp;
	}
	
	public double getUsage() {
		if(this.mileage >= 50000) {
			double temp = this.usage + (this.usage/100 * 9.8);
			return temp;
		}
		else {
			return this.usage;
		}
	}


	public Motor getEngine() {
		return engine;
		
	}

	public void setEngine(Motor engine) {
		this.engine = engine;
	}
	
	
	
}
