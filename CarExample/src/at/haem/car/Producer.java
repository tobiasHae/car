package at.haem.car;

public class Producer {
	
	private String name;
	private String producingCountry;
	private int discount;
	
	
	public Producer(String name, String producingCountry, int discount) {
		this.name = name;
		this.producingCountry = producingCountry;
		this.discount = discount;
	}
	
	public String getname(){
		return this.name;
	}
	
	public int getDiscount() {
		return this.discount;
	}
	
	
	
}
